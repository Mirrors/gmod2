;---------------------------------------------------------------------------
; simple example that demonstrates the "autoselect" command
;---------------------------------------------------------------------------

        !src "../include/gmod2.inc"

ptr = $02

res00 = $f8
res01 = $f9
res02 = $fa

result00 = $0f00
result01 = result00+64
result02 = result01+64

        * = $0801
        !word nextline
        !word 2016 
        !byte $9e
        !byte $32, $30, $36, $31
nextline:
        !byte 0,0,0
        jmp start

start:
        ;sei

        ; clear screen
        ldx #0
-
        lda #$20
        sta $0400,x
        sta $0500,x
        sta $0600,x
        sta $0700,x
        lda #$01
        sta $d800,x
        sta $d900,x
        sta $da00,x
        sta $db00,x
        inx
        bne -

        ; print header
        ldx #0
-
        lda header,x
        sta $0400,x
        inx
        cpx #4*40
        bne -

        ; get the status for each bank
        ldx #0
-
        txa
        pha ; A contains the bank to check

        jsr flash_autoselect
        sta res00
        stx res01
        sty res02

        pla
        tax

        lda res00
        sta result00,x
        lda res01
        sta result01,x
        lda res02
        sta result02,x

        inx
        cpx #64
        bne -

        ; print the status bytes as hex
        ldy #0
--
        tya
        pha

        lda #>($0400+(4*40))
        sta ptr+1
sm1:    lda #<($0400+(4*40))
        sta ptr

sm2:    ldx #0
-
        txa
        pha

        ldy #0
        lda result00,x
        jsr hexout
        lda result01,x
        jsr hexout
        lda result02,x
        jsr hexout

        lda ptr
        clc
        adc #40
        sta ptr
        bcc +
        inc ptr+1
+
        pla
        tax
        inx
sm3:    cpx #8*2
        bne -
    
        lda sm1+1
        clc
        adc #10
        sta sm1+1
    
        lda sm3+1
        sta sm2+1
        clc
        adc #2*8
        sta sm3+1
    
        pla
        tay
        iny
        cpy #4
        bne --

        ; wait forever
        jmp *

hexout:
        pha
        lsr
        lsr
        lsr
        lsr
        tax
        lda hextab,x
        sta (ptr),y
        iny
        pla
        and #$0f
        tax
        lda hextab,x
        sta (ptr),y
        iny
        iny
        rts

hextab: !scr "0123456789abcdef"

header:
             ;0123456789012345678901234567890123456789
        !scr "/-------- manufacturer id (01 = amd)    "
        !scr "!  /----- device id (a4 = 29f040)       "
        !scr "!  !  /-- protection status (01 = on)   "
        !scr "!  !  !                                 "

;-------------------------------------------------------------------------------

        !src "../lib/flash.asm"

        * = $1000   ; to make sure we get a warning when the code becomes too big
