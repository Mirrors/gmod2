
all:
	make -C flash-identify
	make -C eeprom-test
	make -C cartridge-skeleton
	make -C flash-write
	make -C bulk-tester

clean:
	make -C flash-identify clean
	make -C eeprom-test clean
	make -C cartridge-skeleton clean
	make -C flash-write clean
	make -C bulk-tester clean

