
        !src "../include/gmod2.inc"

        *=$8000

        !word launcher
        !word launcher
        !byte $c3 ;c
        !byte $c2 ;b
        !byte $cd ;m
        !byte $38 ;8
        !byte $30 ;0

launcher:
        sei
        stx $d016
        jsr $fda3 ;prepare irq
        jsr $fd50 ;init memory
        jsr $fd15 ;init i/o
        jsr $ff5b ;init video

        ; check if left-arrow key is pressed
        lda #$ff
        sta $dc02
        lda #$00
        sta $dc03
        lda #$7f
        sta $dc00
        lda $dc01

        cmp #$ff - 2
        bne +
        cli
        jmp ($a000) ; start basic
+
        ;ldx #$fb
        ;txs
;---------------------------------
        sei
        ;lda $d011
        ;and #%11101111
        ;sta $d011

        ldx #$00
-
        lda movecode1,x
        sta $0400,x
        inx
        bne -
        jmp movecode
;------------------------------
movecode1:

        !pseudopc $0400 {
movecode:

        ; copy the games main file to memory
        ldy #$00
--
        tya
        pha
-
        ldx #$00
bm1
        lda main_file_start,x
        ldy #$34
        sty $01
bm2
        sta $0801,x
        ldy #$37
        sty $01
        inx
        bne bm1

        inc bm1+2
        inc bm2+2
        lda bm1+2
        cmp #$a0
        bne -
        
        lda #$80
        sta bm1+2

        pla
        tay

        iny
        sty GMOD2REG

        cpy #((main_file_end-main_file_start) / $2000)
        bcc --

        ;disabling the cartridge using bit6 of the register is not recommended,
        ;use the PLA instead
        ;lda #$40
        ;sta GMOD2REG

        ;bank out the cartridge ROM
        lda #$36
        sta $01

        jmp $080d               ; adjust to the start addr of the linked program

        ;jsr $e453 ;load basic vectors
        ;jsr $e3bf ;init basic ram
        ;jsr $a68e
        ;jmp $a7ae ; RUN

        }
movecode3:
;---------------------------------

        ; align to page boundary so we only have to compare the highbyte of the
        ; source address in the above loop
        !align 255,0

main_file_start:
!if FX = 1 {
        !binary "game1.prg",,2
}
!if FX = 2 {
        !binary "game2.prg",,2
}
main_file_end:
