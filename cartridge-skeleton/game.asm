; this is not an actual game :) - just a very simple program to demonstrate
; how to link a seperate program to the example cartridge code

        * = $0801
        !word nextline
        !word 2016 
        !byte $9e
        !byte $32, $30, $36, $31
nextline:
        !byte 0,0,0
        jmp start

start:
        sei

        lda #0
        sta $d011

!if FX = 1 {
        ldx #2
        ldy #6
        lda #5
-
        stx $d020
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        sty $d020
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        sta $d020
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        jmp -
}

!if FX = 2 {
        ldx #0
        ldy #2
        lda #7
--
        stx $d020
        ldx #$00 
-
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        dex
        bne -
        sty $d020
        ldx #$00 
-
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        dex
        bne -
        sta $d020
        ldx #$00 
-
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        bit $eaea
        dex
        bne -
        jmp --
}
