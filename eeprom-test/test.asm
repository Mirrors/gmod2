;-------------------------------------------------------------------------------
; GMod2 EEPROM read/write example
;-------------------------------------------------------------------------------

        !src "../include/gmod2.inc"

        * = $0801
        !word nextline
        !word 2016 
        !byte $9e
        !byte $32, $30, $36, $31
nextline:
        !byte 0,0,0
        jmp start

start:
        ;sei

        ; clear screen
        ldx #0
-
        lda #$20
        sta $0400,x
        sta $0500,x
        sta $0600,x
        sta $0700,x
        lda #$01
        sta $d800,x
        sta $d900,x
        sta $da00,x
        sta $db00,x
        inx
        bne -

        ; create a test pattern
        ldy #0
-
        tya
        sta $0500,y
        iny
        bne -

        ; write a page to the eeprom
        jsr eeprom_reset
        jsr eeprom_write_enable


        ldy #0
-
        tya
        pha

        ldx #0  ; contains upper 2 bits of address
        lsr
        tay     ; contains lower 8 bits of address
        jsr eeprom_write_begin

        pla
        tay
        ; write two bytes to the respective address
        lda $0500,y
        jsr eeprom_write_byte
        iny
        lda $0500,y
        jsr eeprom_write_byte
        jsr eeprom_write_end

        iny
        bne -

        jsr eeprom_write_disable
 
        ; read a page from the eeprom
        jsr eeprom_reset

        ldx #0 ; contains upper 2 bits of address
        ldy #0 ; contains lower 8 bits of address
        jsr eeprom_read_begin

        ; once the address has been set, any number of consecutive bytes can be read
        ldy #0
-
        jsr eeprom_read_byte
        sta $0600,y
        iny
        bne -

        jsr eeprom_read_end

        ; compare the two pages
        ldx #5
        ldy #0
-
        lda $0500,y
        cmp $0600,y
        beq +
        ldx #10
+
        iny
        bne -
        
        lda #0
-
        stx $d020
        sta $d020
        jmp -
 
 
        !src "../lib/eeprom.asm"
 
        * = $1000   ; to make sure we get a warning when the code becomes too big

 
