
This program can be used to test GMOD2 flashrom and eeprom for various
hardware problems.
--------------------------------------------------------------------------------

1) - Output Vendor- and Flash ID

2) - Chip erase
   - check chip for all $ff
   - output "Erase OK/Fail"

3) - write first 9 bytes of bank 0
     $00, $01, $02, $04, $08, $10, $20, $40, $80
   - read back and compare
   - output "Data line check OK/Fail: Expected: xx Read: yy"

4) Addressline check:

   - write following bytes

    $81 to bank  0, $8010
    $82 to bank  0, $8020
    $83 to bank  0, $8040
    $84 to bank  0, $8080
    $85 to bank  0, $8100
    $86 to bank  0, $8200
    $87 to bank  0, $8400
    $88 to bank  0, $8800
    $89 to bank  0, $9000

    $8a to bank  1, $8000
    $8b to bank  2, $8000
    $8c to bank  4, $8000
    $8d to bank  8, $8000
    $8e to bank 16, $8000
    $8f to bank 32, $8000

    - read back and compare
    - output "OK/Error at..."

5) EEPROM check:

    - write 4-byte $55/$aa/$00/$ff pattern
    - read back, compare
    - output OK/Error

6) if no errors, erase Flash and EEPROM
