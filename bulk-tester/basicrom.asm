
        * = $a000
        !word startcopy
        !word startcopy

startcopy:


        sei
        ;lda $d011
        ;and #%11101111
        ;sta $d011

        ldx #$00
-
        lda movecode1,x
        sta $0400,x
        inx
        bne -
        jmp movecode
;------------------------------
movecode1:

        !pseudopc $0400 {
movecode:

        ; copy the games main file to memory
        ldy #$00
--
        tya
        pha
-
        ldx #$00
bm1
        lda main_file_start,x
        ldy #$34
        sty $01
bm2
        sta $0801,x
        ldy #$37
        sty $01
        inx
        bne bm1

        inc bm1+2
        inc bm2+2
        lda bm1+2
        cmp #$c0
        bne -

;         lda #$37
;         sta $01

        jmp $080d               ; adjust to the start addr of the linked program

        }
movecode3:
;---------------------------------

        ; align to page boundary so we only have to compare the highbyte of the
        ; source address in the above loop
        !align 255,0

main_file_start:
        !binary "bulktest.prg",,2

        * = $bfff
        !byte 0
