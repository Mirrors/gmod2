;---------------------------------------------------------------------------
; simple example that demonstrates the "autoselect" command
;---------------------------------------------------------------------------

        !src "../include/gmod2.inc"

ptr     = $02 ; 2

scrptr  = $04 ; 2

scrpos = $10
bank   = $11

res00 = $f8
res01 = $f9
res02 = $fa

buffer2=$0400+(18*40)


        * = $0801
        !word nextline
        !word 2016 
        !byte $9e
        !byte $32, $30, $36, $31
nextline:
        !byte 0,0,0
        jmp start

start:
        ;sei
        lda #$7f
        sta $dc0d
        sta $dd0d

        lda #$00
        sta $d01a
        sta $d020
        sta $d021

        ;------------------------------------------------------
        ; clear screen
        ldx #0
-
        lda #$20
        sta $0400,x
        sta $0500,x
        sta $0600,x
        sta $0700,x
        lda #$01
        sta $d800,x
        sta $d900,x
        sta $da00,x
        sta $db00,x
        inx
        bne -

        ; print header
        ldx #0
-
        lda header,x
        sta $0400,x
        inx
        cpx #4*40
        bne -

        ;------------------------------------------------------
        ; 1) - Output Vendor- and Flash ID

        lda #0 ; A contains the bank to check
        jsr flash_autoselect
        sta res00
        stx res01
        sty res02

        ; print the status bytes as hex
        ldx #>($0400+(4*40))
        lda #<($0400+(4*40))
        jsr setscrptr
        ldy #0
        sty scrpos

        ldy #0
        lda res00
        jsr hexout
        inc scrpos
        lda res01
        jsr hexout
        inc scrpos
        lda res02
        jsr hexout

        jsr crout
        jsr crout

        ;------------------------------------------------------
        ; 2) - Chip erase
        ;    - check chip for all $ff
        ;    - output "Erase OK/Fail"

;         ldx #>($0400+(5*40))
;         lda #<($0400+(5*40))
;         jsr setscrptr

        lda #<strErase
        ldx #>strErase
        jsr strout

        jsr flash_reset

        ; before we can write to the flash, we must erase it
        inc $07e7   ; to indicate progress
        jsr flash_chiperase
        dec $07e7   ; to indicate progress

        lda #0
        sta bank
---
        sta $de00
        sta $07e7   ; to indicate progress

        ; check
        lda #<$8000
        sta ptr
        lda #>$8000
        sta ptr+1

        ldx #$20
--
        ldy #0
-
        lda (ptr),y
        sta $0400+(18*40),y
        cmp #$ff
        bne fail2
        iny
        bne -

        inc ptr+1
        dex
        bne --

        inc bank
        lda bank
        cmp #$40
        bne ---

        lda #<strOK
        ldx #>strOK
        jsr strout
        jsr crout

        jmp do3
fail2:
        lda #<strFail
        ldx #>strFail
        jsr strout

        lda #10
        sta $d020
        jmp *

        ;------------------------------------------------------
        ; 3) - write first 9 bytes of bank 0
        ;      $00, $01, $02, $04, $08, $10, $20, $40, $80
        ;    - read back and compare
        ;    - output "Data line check OK/Fail: Expected: xx Read: yy"
do3:
        ldy #0
        lda #<strDataLineCheck
        ldx #>strDataLineCheck
        jsr strout

        ldx #0
-
        txa
        pha

        inc $07e6   ; to indicate progress
        ; set byte program command and flash address
        lda #$00
        ldy #$00
faddl:  ldx #$00
        jsr flash_byteprogram_setaddr

        inc $07e7   ; to indicate progress
cadd:   lda flashdata3
        ; program one byte
        jsr flash_byteprogram_put
        bcs failure3a

        inc cadd+1
        bne +
        inc cadd+2
+
        inc faddl+1

        pla
        tax

        inx
        cpx #9
        bne -

        ; verify
        ldx #0
        stx $de00   ; bank 0
-
        lda $8000,x
        cmp flashdata3,x
        bne failure3b
        inx
        cpx #9
        bne -

        lda #<strOK
        ldx #>strOK
        jsr strout
        jsr crout

        jmp do4

failure3a:
        ; flash error
        lda #<strWriteError
        ldx #>strWriteError
        jsr strout
        jsr crout

        lda #10
        sta $d020
        jmp *

failure3b:
        jmp failure3b_far

flashdata3:
        !byte $00, $01, $02, $04, $08, $10, $20, $40, $80

        ;------------------------------------------------------
        ; 4) Addressline check:
        ;    - write following bytes
        ;    $81 to bank  0, $8010  flash addr 000010
        ;    $82 to bank  0, $8020             000020
        ;    $83 to bank  0, $8040             000040
        ;    $84 to bank  0, $8080             000080
        ;    $85 to bank  0, $8100             000100
        ;    $86 to bank  0, $8200             000200
        ;    $87 to bank  0, $8400             000400
        ;    $88 to bank  0, $8800             000800
        ;    $89 to bank  0, $9000             001000
        ;    $8a to bank  1, $8000             002000
        ;    $8b to bank  2, $8000             004000
        ;    $8c to bank  4, $8000             008000
        ;    $8d to bank  8, $8000             010000
        ;    $8e to bank 16, $8000             020000
        ;    $8f to bank 32, $8000             040000
        ;    - read back and compare
        ;    - output "OK/Error at..."
do4:

        lda #<strAddressLineCheck
        ldx #>strAddressLineCheck
        jsr strout

        ldx #0
-

        lda value4,x
        sta data4

        lda faddrlo4,x
        sta faddrl4
        lda faddrhi4,x
        sta faddrh4
        lda faddrhhi4,x
        sta faddrhh4

        txa
        pha

        inc $07e6   ; to indicate progress
        ; set byte program command and flash address
        ; A : upper 3 bits
        ; Y : bits 15-8
        ; X : bits 7-0
faddrhh4=*+1
        lda #$00
faddrh4=*+1
        ldy #$00
faddrl4=*+1
        ldx #$00
        jsr flash_byteprogram_setaddr

        inc $07e7   ; to indicate progress
data4=*+1
        lda #0
        ; program one byte
        jsr flash_byteprogram_put
        bcs failure3a

        pla
        tax

        inx
        cpx #$0f
        bne -

        ; verify
        ldx #0
-
        lda banks4,x
        sta $de00
        lda addrlo4,x
        sta addr4
        lda addrhi4,x
        sta addr4+1
addr4=*+1
        lda $dead
        cmp value4,x
        bne failure4b
        inx
        cpx #$0f
        bne -

        lda #<strOK
        ldx #>strOK
        jsr strout
        jsr crout

        jmp do5

failure4a:
        ; flash error
        lda #<strWriteError
        ldx #>strWriteError
        jsr strout
        jsr crout

        lda #10
        sta $d020
        jmp *

failure4b:
        jmp failure4b_far

faddrlo4:
            !byte $10, $20, $40, $80, $00, $00, $00, $00
            !byte $00, $00, $00, $00, $00, $00, $00
faddrhi4:
            !byte $00, $00, $00, $00, $01, $02, $04, $08
            !byte $10, $20, $40, $80, $00, $00, $00
faddrhhi4:
            !byte $00, $00, $00, $00, $00, $00, $00, $00
            !byte $00, $00, $00, $00, $01, $02, $04

value4:     !byte $81, $82, $83, $84, $85, $86, $87, $88
            !byte $89, $8a, $8b, $8c, $8d, $8e, $8f

addrlo4:    !byte $10, $20, $40, $80, $00, $00, $00, $00
            !byte $00, $00, $00, $00, $00, $00, $00

addrhi4:    !byte $80, $80, $80, $80, $81, $82, $84, $88
            !byte $90, $80, $80, $80, $80, $80, $80

banks4:     !byte $00, $00, $00, $00, $00, $00, $00, $00
            !byte $00, $01, $02, $04, $08, $10, $20

        ;------------------------------------------------------
        ; 5) EEPROM check:
        ;    - write 4-byte $55/$aa/$00/$ff pattern
        ;    - read back, compare
        ;    - output OK/Error
do5:
        lda #<strEEPROMCheck
        ldx #>strEEPROMCheck
        jsr strout

        ; write a page to the eeprom
        jsr eeprom_reset

        ldx #0
--
        txa
        pha
        lsr
        sta eepromhi

        jsr eeprom_write_enable

        ldy #0
-
        tya
        pha
eepromhi=*+1
        ldx #0  ; contains upper 2 bits of address
        lsr
        clc
eepromadd=*+1
        adc #$00
        tay     ; contains lower 8 bits of address
        jsr eeprom_write_begin

        pla
        tay
        ; write two bytes to the respective address
        lda buffer1,y
        jsr eeprom_write_byte
        iny
        lda buffer1,y
        jsr eeprom_write_byte

        jsr eeprom_write_end

        iny
        bne -

        jsr eeprom_write_disable
        
        lda eepromadd
        eor #$80
        sta eepromadd

        pla
        tax
        inx
        cpx #8
        bne --
 
        ; read a page from the eeprom
        jsr eeprom_reset

        ldx #0
--
        txa
        pha
        lsr
        ;sta eepromhi2
        tax

        ;ldx #0 ; contains upper 2 bits of address
eepromadd2=*+1
        ldy #0 ; contains lower 8 bits of address
        jsr eeprom_read_begin

        ; once the address has been set, any number of consecutive bytes can be read
        ldy #0
-
        jsr eeprom_read_byte
        sta buffer2,y
        iny
        bne -

        jsr eeprom_read_end

        ; compare the two pages
        ldy #0
-
        lda buffer1,y
        cmp buffer2,y
        bne failure5
        iny
        bne -

        lda eepromadd2
        eor #$80
        sta eepromadd2

        pla
        tax
        inx
        cpx #8
        bne --

        lda #<strOK
        ldx #>strOK
        jsr strout
        jsr crout

        jmp do6

failure5:
        ; eeprom error
        lda #<strFail
        ldx #>strFail
        jsr strout
        jsr crout

        lda #10
        sta $d020
        jmp *

        ;------------------------------------------------------
        ; 6) if no errors, erase Flash and EEPROM
do6:
        ldy #0
        lda #<strErase
        ldx #>strErase
        jsr strout

        jsr flash_reset

        ; before we can write to the flash, we must erase it
        inc $07e7   ; to indicate progress
        jsr flash_chiperase
        dec $07e7   ; to indicate progress

        jsr eeprom_reset
        jsr eeprom_write_enable
        jsr eeprom_erase_all

        lda #<strDone
        ldx #>strDone
        jsr strout

        lda #$05
        sta $d020

        ; wait forever
        jmp *

;-------------------------------------------------------------------------------

setscrptr:
        stx scrptr+1
        sta scrptr
        rts

hexout:
        pha
        lsr
        lsr
        lsr
        lsr
        tax
        ldy scrpos
        lda hextab,x
        sta (scrptr),y
        iny
        pla
        and #$0f
        tax
        lda hextab,x
        sta (scrptr),y
        iny
        sty scrpos
        rts

hextab: !scr "0123456789abcdef"

strout:
        stx str+1
        sta str

        ldy scrpos
        ldx #0
-
str=*+1
        lda $dead,x
        beq +
        sta (scrptr),y
        iny
        inx
        bne -
+
        sty scrpos
        rts

crout:
        lda scrptr
        clc
        adc #40
        sta scrptr
        bcc +
        inc scrptr+1
+
        ldy #0
        sty scrpos
        rts

;-------------------------------------------------------------------------------

        !src "../lib/flash.asm"
        !src "../lib/eeprom.asm"

        !align 255,0
buffer1:
        !for n,0,(256/4) {
            !byte $55, $aa, $00, $ff
        }

        * = $1000   ; to make sure we get a warning when the code becomes too big

;-------------------------------------------------------------------------------

header:
             ;0123456789012345678901234567890123456789
        !scr "/-------- manufacturer id (01 = amd)    "
        !scr "!  /----- device id (a4 = 29f040)       "
        !scr "!  !  /-- protection status (01 = on)   "
        !scr "!  !  !                                 "

strErase:               !scr "erase...",0
strDataLineCheck:       !scr "data line check...",0
strAddressLineCheck:    !scr "address line check...",0
strEEPROMCheck:         !scr "eeprom check...",0

strOK:          !scr "ok",0
strFail:        !scr "fail",0
strDone:        !scr "done",0
strWriteError:  !scr "write error",0
strVerifyError: !scr "verify error",0

strExpected:    !scr " expected:",0
strRead:        !scr " read:",0
strAt:          !scr " bank:",0
strAddr:        !scr " addr:",0

;-------------------------------------------------------------------------------

        ;    - output "Fail: Expected: xx Read: yy"

        ; value that was read in A
        ; index to table in X
failure3b_far:
        sta readval3b
        lda flashdata3,x
        sta expectval3b

        ; verify error
        lda #<strVerifyError
        ldx #>strVerifyError
        jsr strout
        jsr crout

        lda #<strExpected
        ldx #>strExpected
        jsr strout

expectval3b=*+1
        lda #0
        jsr hexout

        lda #<strRead
        ldx #>strRead
        jsr strout

readval3b=*+1
        lda #0
        jsr hexout

        jsr crout

        lda #10
        sta $d020
        jmp *

;-------------------------------------------------------------------------------

failure4b_far:
        sta readval4b
        lda value4,x
        sta expectval4b

        lda banks4,x
        sta expectbank4
        lda addrlo4,x
        sta expectaddrlo4
        lda addrhi4,x
        sta expectaddrhi4

        ; verify error
        lda #<strVerifyError
        ldx #>strVerifyError
        jsr strout
        jsr crout

        lda #<strExpected
        ldx #>strExpected
        jsr strout

expectval4b=*+1
        lda #0
        jsr hexout

        lda #<strRead
        ldx #>strRead
        jsr strout

readval4b=*+1
        lda #0
        jsr hexout

        lda #<strAt
        ldx #>strAt
        jsr strout

expectbank4=*+1
        lda #0
        jsr hexout

        lda #<strAddr
        ldx #>strAddr
        jsr strout

expectaddrhi4=*+1
        lda #0
        jsr hexout

expectaddrlo4=*+1
        lda #0
        jsr hexout
        lda #10
        sta $d020
        jmp *
