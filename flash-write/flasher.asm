;---------------------------------------------------------------------------
; simple example that demonstrates how to write to the flash ROM
;---------------------------------------------------------------------------

        !src "../include/gmod2.inc"

        * = $0801
        !word nextline
        !word 2016 
        !byte $9e
        !byte $32, $30, $36, $31
nextline:
        !byte 0,0,0
        jmp start

start:
        ;sei

        ; clear screen
        ldx #0
-
        lda #$20
        sta $0400,x
        sta $0500,x
        sta $0600,x
        sta $0700,x
        lda #$01
        sta $d800,x
        sta $d900,x
        sta $da00,x
        sta $db00,x
        inx
        bne -

        jsr flash_reset

        ; before we can write to the flash, we must erase it
        inc $0400   ; to indicate progress
        jsr flash_chiperase
        inc $0401   ; to indicate progress

        ldy #0
--
        ldx #0
-
        tya
        pha
        txa
        pha

        inc $0402   ; to indicate progress
        ; set byte program command and flash address
        lda #$00
faddh:  ldy #$00
faddl:  ldx #$00
        jsr flash_byteprogram_setaddr

        inc $0403   ; to indicate progress
cadd:   lda cart_file_start
        ; program one byte
        jsr flash_byteprogram_put
        bcs failure

        inc cadd+1
        bne +
        inc cadd+2
+
        inc faddl+1
        bne +
        inc faddh+1
+
        pla
        tax
        pla
        tay

        inx
        bne -

        sty $0404   ; to indicate progress

        iny
        cpy #$20
        bne --

-
        inc $d020
        dec $d020
        jmp -

failure:
        lda #2
        sta $d020
        jmp *

;-------------------------------------------------------------------------------

        !src "../lib/flash.asm"

        * = $1000   ; to make sure we get a warning when the code becomes too big

;---------------------------------

        ; align to page boundary so we only have to compare the highbyte of the
        ; source address in the above loop
        !align 255,0

cart_file_start:
!if FX = 1 {
        !binary "../cartridge-skeleton/gamecart1.bin"
}
!if FX = 2 {
        !binary "../cartridge-skeleton/gamecart2.bin"
}
