;============================================================
; GMOD2 Flash Runtime
; Version 1.0.0
; (C) 2016 by Chester Kollschen (original code)
;             Tobias Korbmacher (cleanup and comments)
;------------------------------------------------------------

flash_read_bank:        !byte $00
flash_write_bank:       !byte $00
flash_ref:              !byte $00

;------------------------------------------------------------
flash_write0000:
                        ldx #$C0
                        stx $DE00
                        sta $E000
                        rts

flash_write2AAA:
                        ldx #$C1
                        stx $DE00
                        sta $EAAA
                        rts

flash_write5555:
                        ldx #$C2
                        stx $DE00
                        sta $F555
                        rts

flash_read5555:
                        ldx #$02
                        stx $DE00
                        lda $9555
                        rts
;------------------------------------------------------------
; set the address for programming
; A : upper 3 bits
; Y : bits 15-8
; X : bits 7-0
flash_setaddr:
                        and #$07
                        sta flash_read_bank
                        tya
                        asl
                        rol flash_read_bank
                        asl
                        rol flash_read_bank
                        asl
                        rol flash_read_bank
                        lda flash_read_bank
                        ora #$c0
                        sta flash_write_bank
                        tya
                        and #$1f
                        ora #$80
                        sta flash_put_sm2+2
                        sta flash_put_sm3+2
                        ora #$e0
                        sta flash_put_sm1+2
                        stx flash_put_sm1+1
                        stx flash_put_sm2+1
                        stx flash_put_sm3+1
                        rts

;------------------------------------------------------------
; program the byte in A
flash_put:
                        pha
                        lda flash_write_bank
                        sta GMOD2REG
                        pla
flash_put_sm1:
                        sta $0000
                        and #$80
                        sta flash_ref
                        lda flash_read_bank
                        sta GMOD2REG
-
flash_put_sm2:
                        lda $0000
                        eor flash_ref
                        bpl flash_put_pass
                        and #$20
                        bne -
flash_put_sm3:
                        lda $0000
                        eor flash_ref
                        bpl flash_put_pass
flash_put_fail:
                        sec
                        rts
flash_put_pass:
                        clc
                        rts
                        
;------------------------------------------------------------
flash_reset:
                        lda #$f0
                        jmp flash_write0000

;------------------------------------------------------------
; detect type and manufacturer
flash_autoselect:
                        ;sei
                        and #$3f
                        pha

                        lda #$AA
                        jsr flash_write5555
                        lda #$55
                        jsr flash_write2AAA
                        lda #$90                ; autoselect
                        jsr flash_write5555

                        pla
                        sta GMOD2REG

                        lda $8000  ; $01 Manufacturer ID
                        ldx $8001  ; $A4 Chip ID
                        ldy $8002  ; $00 Sector Protection status (0 = not protected)
                        ;cli
                        rts

;------------------------------------------------------------
; a typical write sequence looks like this:
;
; flash_chiperase
;                           <--+
; flash_byteprogram_setaddr    |
; flash_byteprogram_put        |
;                  ------------+

;------------------------------------------------------------
; erase the entire flash rom
flash_chiperase:
                        ;sei
                        lda #$AA
                        jsr flash_write5555
                        lda #$55
                        jsr flash_write2AAA
                        lda #$80                ; erase
                        jsr flash_write5555

                        lda #$AA
                        jsr flash_write5555
                        lda #$55
                        jsr flash_write2AAA
                        lda #$10                ; chip erase
                        jsr flash_write5555
-
                        jsr flash_read5555
                        bpl -
                        ;cli
                        rts
 
;------------------------------------------------------------
; send byte-program command and set address
flash_byteprogram_setaddr:

                        pha
                        txa
                        pha

                        lda #$AA
                        jsr flash_write5555
                        lda #$55
                        jsr flash_write2AAA
                        lda #$a0                ; byte program
                        jsr flash_write5555

                        pla
                        tax
                        pla
                        jmp flash_setaddr

;------------------------------------------------------------
flash_byteprogram_put = flash_put
