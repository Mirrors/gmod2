;============================================================
; GMOD2 EEPROM Runtime
; Version 1.0.0
; (C) 2016 by Chester Kollschen (original code)
;             Tobias Korbmacher (cleanup and comments)
;------------------------------------------------------------

; copy of the value stored into the cartridge register
GMOD2REG_SHADOW         !byte $00
eeprom_data_temp        !byte $00

;------------------------------------------------------------
eeprom_reset:
                        lda #$00
                        sta GMOD2REG_SHADOW
                        sta GMOD2REG
                        rts

;------------------------------------------------------------
; set chip select = low
eeprom_cs_lo:
                        lda GMOD2REG_SHADOW
                        and #255-(EEPROM_SELECT)
                        sta GMOD2REG_SHADOW
                        sta GMOD2REG
                        rts

;------------------------------------------------------------
; set chip select = high
eeprom_cs_high:
                        lda GMOD2REG_SHADOW
                        ora #(EEPROM_SELECT)
                        sta GMOD2REG_SHADOW
                        sta GMOD2REG
                        rts

;------------------------------------------------------------
; set clock = low
eeprom_clk_lo:
                        lda GMOD2REG_SHADOW
                        and #255-(EEPROM_CLOCK)
                        sta GMOD2REG_SHADOW
                        sta GMOD2REG
                        rts

;------------------------------------------------------------
; set clock = high
eeprom_clk_high:
                        lda GMOD2REG_SHADOW
                        ora #(EEPROM_CLOCK)
                        sta GMOD2REG_SHADOW
                        sta GMOD2REG
                        rts

;------------------------------------------------------------
eeprom_wait_ready:
                        jsr eeprom_cs_lo
                        jsr eeprom_cs_high

                        lda #(EEPROM_DATAOUT)
-
                        bit GMOD2REG
                        beq -
                        rts

;------------------------------------------------------------
; A: contains the bits to send, msb first
; X: number of bits to send
eeprom_send_bits:
                        sta eeprom_data_temp
-
                        asl eeprom_data_temp

                        lda GMOD2REG_SHADOW
                        and #255-(EEPROM_DATAIN)
                        bcc +
                        ora #(EEPROM_DATAIN)
+
                        sta GMOD2REG_SHADOW
                        sta GMOD2REG

                        jsr eeprom_clk_high
                        jsr eeprom_clk_lo

                        dex
                        bne -

                        rts

;------------------------------------------------------------
; returns: A: the byte the was read
eeprom_receive_byte:
                        ldx #$08
-
                        jsr eeprom_clk_high
                        jsr eeprom_clk_lo

                        lda GMOD2REG
                        asl
                        rol eeprom_data_temp

                        dex
                        bne -

                        lda eeprom_data_temp
                        rts

;------------------------------------------------------------
eeprom_write_enable:
                        jsr eeprom_cs_high

                        lda #%10011000          ; 10011 = startbit, write enable
                        ldx #$05
                        jsr eeprom_send_bits

                        lda #%00000000          ; dummy
                        ldx #$08
                        jsr eeprom_send_bits

                        jmp eeprom_cs_lo

;------------------------------------------------------------
eeprom_write_disable:
                        jsr eeprom_cs_high

                        lda #%10000000          ; 10000 = startbit, write diable
                        ldx #$05
                        jsr eeprom_send_bits

                        lda #%00000000          ; dummy
                        ldx #$08
                        jsr eeprom_send_bits

                        jmp eeprom_cs_lo

;------------------------------------------------------------
eeprom_erase_all:
                        jsr eeprom_cs_high

                        lda #%10010000          ; 10010 = startbit, erase all
                        ldx #$05
                        jsr eeprom_send_bits

                        lda #%00000000          ; dummy
                        ldx #$08
                        jsr eeprom_send_bits

                        jsr eeprom_wait_ready
                        jmp eeprom_cs_lo

;------------------------------------------------------------
; a typical read sequence looks like this:
;
; eeprom_reset
; eeprom_read_begin
;                      <----+
; eeprom_read_byte          |
;              -------------+
; eeprom_read_end
;------------------------------------------------------------

;------------------------------------------------------------
; x = lower 2 bits contain the 2 highest bits of the address
; y = lower 8 bits of the address
eeprom_read_begin:
                        jsr eeprom_cs_high

                        tya
                        pha

                        txa
                        and #$03
                        asl
                        asl
                        asl
                        ora #%11000000          ; 110xx = startbit, read, 2 bits addr
                        ldx #$05
                        jsr eeprom_send_bits

                        pla                     ; 8 bits addr
                        ldx #$08
                        jmp eeprom_send_bits

eeprom_read_byte        = eeprom_receive_byte
eeprom_read_end         = eeprom_cs_lo

;------------------------------------------------------------
; a typical write sequence looks like this:
;
; eeprom_reset
; eeprom_write_enable
;                      <----+
; eeprom_write_begin        |
; eeprom_write_byte         |
; eeprom_write_byte         |
; eeprom_write_end          |
;              -------------+
; eeprom_write_disable
;------------------------------------------------------------

;------------------------------------------------------------
; x = lower 2 bits contain the 2 highest bits of the address
; y = lower 8 bits of the address
eeprom_write_begin:
                        jsr eeprom_cs_high

                        tya
                        pha

                        txa
                        and #$03
                        asl
                        asl
                        asl
                        ora #%10100000          ; 101xx = startbit, read, 2 bits addr
                        ldx #$05
                        jsr eeprom_send_bits

                        pla                     ; 8 bits addr
                        ;ldx #$08
                        ;jmp eeprom_send_bits
                        ; fall through
eeprom_write_byte:
                        ldx #$08
                        jmp eeprom_send_bits

eeprom_write_end:
                        jsr eeprom_wait_ready
                        jmp eeprom_cs_lo
 
